from setuptools import setup

setup(
    name='simpplr_aws_cdc',
    version='master1.0',
    packages=['src', 'src.com', 'src.com.simpplr', 'src.com.simpplr.common', 'src.com.simpplr.common.utils',
              'src.com.simpplr.producer', 'src.com.simpplr.consumers', 'src.com.simpplr.consumers.models'],
    url='simpplr.com',
    license='',
    author='sampatbudankayala',
    author_email='',
    description=''
)
