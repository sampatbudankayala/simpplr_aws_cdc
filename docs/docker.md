####Docker run command for local setup

```docker-run
docker run -d -p 2181:2181 -p 3030:3030 -p 8081-8083:8081-8083 \
           -p 9581-9585:9581-9585 -p 9092:9092 -e ADV_HOST=127.0.0.1 \
           -e RUNNING_SAMPLEDATA=1 -e CONNECTORS=debezium-mysql lensesio/fast-data-dev
```
Visit [click-me](http://localhost:3030) to get into the fast-data-dev environment

NOTE: https://github.com/lensesio/fast-data-dev

####Docker helper commands

To delete all containers including its volumes use,
* ```docker rm -vf $(docker ps -a -q)```

To delete all the docker images,
* ```docker rmi -f $(docker images -a -q)```

To get into docker bash,
* ```docker exec -it <Conatiner_id> bash```

####Check the information of Mysql 

To get the info of mysql env installed in mac
* ```brew info mysql```

To check the status of mysql env is running or not
* ```mysql.server status```

To start the mysql server installed in mac
* ```mysql.server start```

To login into mysql server using pass 
* ```myslq -u root -p```

If you get below error , it might be that the mysql server is not started

```mysql -u root -p
Enter password:
ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2)
```


```docker run -it --net=host --mount type=bind,source=/tmp/log,target=/opt/app/simpplr_aws_cdc/log/connector_log sampatdocker/cdc_ubuntu_image_simpplr_tt```

```docker build . -t sampatdocker/cdc_ubuntu_image_simpplr_tt ```


```docker run -it --net=host \
                --env-file ./env.list \
         -e CONNECTOR_INI_FILE_NAME="cdc_producer_config.ini"\
         -e CONNECTOR_PROPERTY_FILE_NAME="mysql_debezium_jdbc_connection" -e CONNECTOR_TYPE="source" \
         -e CONNECTOR_NAME="Mysql_env_simpple1" --mount type=bind,source=/tmp/log,target=/opt/app/simpplr_aws_cdc/log/connector_log \
          sampatdocker/cdc_ubuntu_image_simpplr_env_changes```