"""Defines core consumers functionality"""
from confluent_kafka import Consumer, OFFSET_BEGINNING,OFFSET_END
from confluent_kafka.avro import AvroConsumer
from src.com.simpplr.common import connection_config
import asyncio


class KafkaConsumer:
    """Defines the base kafka consumers class"""

    def __init__(
        self,
        topic_name_pattern,
        message_handler,
        logger,
        is_avro=True,
        offset_earliest=False,
        sleep_secs=1.0,
        consume_timeout=0.1,
    ):
        """Creates a consumers object for asynchronous use"""
        self.topic_name_pattern = topic_name_pattern
        self.message_handler = message_handler
        self.__logger = logger
        self.sleep_secs = sleep_secs
        self.consume_timeout = consume_timeout
        self.offset_earliest = offset_earliest

        self.broker_properties = {
            'BROKER_URL':connection_config.Connections.KAFKA_BROKER,
            'SCHEMA_REGISTRY': connection_config.Connections.SCHEMA_REGISTRY,
            'KAFKA_REST_PROXY': connection_config.Connections.REST_PROXY
        }

        # Create the Consumer, using the appropriate type.
        if is_avro is True:
            self.broker_properties['SCHEMA_REGISTRY'] = "http://localhost:8081"
            self.consumer = AvroConsumer({
                'bootstrap.servers': self.broker_properties['BROKER_URL'],
                'group.id': connection_config.KAFKA_CONSUMER_GROUP,
                'schema.registry.url': self.broker_properties['SCHEMA_REGISTRY'],
                'auto.offset.reset': 'earliest'
            })
        else:
            self.consumer = Consumer({
                'bootstrap.servers': self.broker_properties['BROKER_URL'],
                'group.id': connection_config.KAFKA_CONSUMER_GROUP,
                'auto.offset.reset': 'earliest'
            })

        # Configure the AvroConsumer and subscribe to the topics. Make sure to think about
        # how the `on_assign` callback should be invoked.
        self.consumer.subscribe([self.topic_name_pattern], on_assign=self.on_assign)

    def on_assign(self, consumer, partitions):
        """Callback for when topic assignment takes place"""
        # If the topic is configured to use `offset_earliest` set the partition offset to
        # the beginning or earliest
        for partition in partitions:
            partition.offset = OFFSET_END
            self.__logger.info("The partition offset is set to begining : {}".format(partition.offset))
            self.__logger.info("partition offset set to beginning for %s", self.topic_name_pattern)

        self.__logger.info("partitions assigned for %s", self.topic_name_pattern)
        consumer.assign(partitions)

    async def consume(self):
        """Asynchronously consumes data from kafka topic"""
        while True:
            num_results = 1
            while num_results > 0:
                num_results = self._consume()
                self.__logger.info("Message process successfull then return 1 or 0 : The return value is : {}".format(num_results))
            await asyncio.sleep(0.1)

    def _consume(self):
        """Polls for a message. Returns 1 if a message was received, 0 otherwise"""
        # Poll Kafka for messages. Make sure to handle any errors or exceptions.
        # Additionally, make sure you return 1 when a message is processed, and 0 when no message
        # is retrieved.
        self.__logger.info("Waiting for the message ...............")
        message = self.consumer.poll(self.consume_timeout)
        if message is None:
            self.__logger.info("no message received by consumers")
            return 0
        elif message.error() is not None:
            self.__logger.info(f"error from consumers {message.error()}")
            return 0
        else:
            self.message_handler(self,message)
            print("Message_Value: {} ".format(message.value()))
            self.__logger.info(f"consumed message : key : {message.key()}: value: {message.value()} : offset: {message.offset()} : partition: {message.partition()} : timestamp:{message.timestamp()}")
            return 1


    def close(self):
        """Cleans up any open kafka consumers"""
        self.__logger.info("closing consumer for topic {}".format(self.topic_name_pattern))
        self.consumer.close()