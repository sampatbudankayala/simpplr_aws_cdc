"""Configures a Kafka Connector for Postgres Station data"""
import json

from src.com.simpplr.common.connection_config import Connections
from src.com.simpplr.common import config_parser as confParser,logger as cdc_logger
from src.com.simpplr.common.utils.kafka_cdc_utils import ProducerUtils
import src.com.simpplr.producer


def parse_args():
    '''
        The below argument parser ,
        will parse all the command driven parameters
        which is passed in the python callable script.

    '''
    import argparse
    parser = argparse.ArgumentParser(description='Processing Command Line Arguments Passed from the Python Run Command')
    parser.add_argument('-cnf_file_pth', '--conf_file_path',
                        dest="confFilePath", help='Configuration File Path',
                        required=True)
    parser.add_argument('-prop_file_name','--properties_file_name',
                       dest="propFileName", help='Connector properties File Name')
    parser.add_argument('-conn_type','--connector_type',
                        dest="connType" ,default="source" , help='Connector type can be either [source] or [sink] , with default set as [source]')
    parser.add_argument('-conn_name','--connector_name',
                        dest="connName", help='Connector Name')
    parser.add_argument('-f', '--force',
                        help='Overwrite destination file if it already exists')

    return parser.parse_args()

def main():

    arguments = parse_args()

    '''
            Accessing the Parsed arguments from python command line.
    '''
    cdc_config_path=arguments.confFilePath

    '''
           Accessing the section based arguments from the ".ini".
    '''
    confParser.config_file_path = cdc_config_path
    confParser.set_config()

    logger = cdc_logger.getLoggerLvl(__name__)
    logger.info("CONFIG_FILE_PATH : {}".format(cdc_config_path))
    connector_prop_filename = arguments.propFileName
    logger.info("PROPERTY_FILE_NAME : {}".format(connector_prop_filename))
    connector_name = arguments.connName
    logger.info(" The connector name is  : {}".format(connector_name))
    connector_uri = Connections.CONNECT
    logger.info("CONNECTOR URI : {}".format(connector_uri))

    logger.info("Calling Method : validate_connector_exists from kafka_cdc_utils!!!!!")
    producer_utils = ProducerUtils(logger)
    if(producer_utils.validate_connector_exists(connector_uri,connector_name)):
        logger.error("Connector with name : {}  already exists !!! Please chose a different connector name".format(connector_name))
        exit(1)
    else:
        logger.info("Connector with name : {} does not exists!!! Continuing to create a connector with the properties in file : {}".format(connector_name,connector_prop_filename))

    project_home_dir = confParser.config_section_map("default").get("project_home").strip()
    connector_type = arguments.connType
    connector_properties_file_path = "{}/resources/connector_properties_files/{}".format(project_home_dir,connector_type)


    responseObj=producer_utils.validate_post_request(connector_uri,connector_name,connector_properties_file_path,connector_prop_filename)

    try:
        responseObj.raise_for_status()
    except:
        logger.exception(f"failed creating connector : {json.dumps(responseObj.json(),indent=2)}")
        exit(1)

    logger.info("connector : {} created successfully".format(connector_name))
    logger.info("Use kafka-console-consume and kafka-topics to see data !!!")

if __name__ == "__main__":
    main()
















