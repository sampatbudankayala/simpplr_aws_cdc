from configparser import ConfigParser


from pathlib import Path

PROJECT_HOME = Path(__file__).parent.parent.parent.parent.parent # This is your Project Roo

parser = ConfigParser()
parser.read("{}/conf/cdc_producer_config.ini".format(PROJECT_HOME))
parser.set("default","project_home",str(PROJECT_HOME))

LOG_DIR="{}/log/connector_log".format(str(PROJECT_HOME))
parser.set("file_locations","log_dir",LOG_DIR)
PROPERTIES_FILE_DIRECTORY="{}/resources".format(str(PROJECT_HOME))
parser.set("file_locations","properties_file_dir",PROPERTIES_FILE_DIRECTORY)



with open('{}/conf/cdc_producer_config.ini'.format(PROJECT_HOME), 'w') as configfile:
    parser.write(configfile)