from src.com.simpplr.consumers.models import aws_sns
from src.com.simpplr.consumers.consumer import KafkaConsumer
from src.com.simpplr.consumers import topic_check
import argparse
import asyncio
from src.com.simpplr.common import config_parser as confParser,logger as cdc_logger



def parse_args():
    parser = argparse.ArgumentParser(description='Processing Command Line Arguments Passed from the Python Run Command')
    parser.add_argument('-cnf_file_pth', '--conf_file_path',
                        dest="confFilePath", help='Configuration File Path',
                        required=True)
    parser.add_argument('-topic_name','--kafka_topic_name',
                       dest="kafkaTopicName", help='KafkaTopicName',required=True)
    return parser.parse_args()


def main():
    arguments = parse_args()
    '''
                Accessing the Parsed arguments from python command line.
    '''

    cdc_config_path = arguments.confFilePath
    confParser.config_file_path = cdc_config_path
    confParser.set_config()

    logger = cdc_logger.getLoggerLvl(__name__)
    logger.info("CONFIG_FILE_PATH : {} ".format(cdc_config_path))
    kafka_topic_name = arguments.kafkaTopicName
    logger.info("KAFKA_TOPIC_NAME : {} ".format(kafka_topic_name))

    logger.info("Checking the topic {} , already exists or not !!!".format(kafka_topic_name))
    if topic_check.topic_exists(kafka_topic_name) is False:
        logger.fatal(
            "Ensure that the topic {} has been created !!!".format(kafka_topic_name)
        )
        exit(1)


    consumers = [
            KafkaConsumer(kafka_topic_name,
                      aws_sns.SendSnsNotification.process_message,
                      logger,
                      offset_earliest=True,
                      is_avro=False,
                      )
    ]

    try:
        for consumer in consumers:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(consumer.consume())
    except KeyboardInterrupt as e:
            logger.info("shutting down server for consumner topic {}".format(consumer.topic_name_pattern))
            loop.close()
            for consumer in consumers:
                consumer.close()


if __name__ == "__main__":
    main()


