from configparser import ConfigParser

from pathlib import Path

PROJECT_HOME = Path(__file__).parent.parent.parent.parent # This is your Project Roo

print(PROJECT_HOME)

parser = ConfigParser()
parser.read("{}/conf/cdc_consumer_config.ini".format(PROJECT_HOME))
parser.set("default","project_home",str(PROJECT_HOME))
LOG_DIR="{}/log/consumer_log".format(str(PROJECT_HOME))
parser.set("file_locations","log_dir",LOG_DIR)

with open('{}/conf/cdc_consumer_config.ini'.format(PROJECT_HOME), 'w') as configfile:
    parser.write(configfile)