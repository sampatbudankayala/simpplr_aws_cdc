import logging
import uuid
from datetime import datetime


time_string = datetime.now().strftime("%Y%m%d_%H%M%S")
u_id = str(uuid.uuid4().hex.upper()[0:6])

import src.com.simpplr.common.config_parser as confParser



'''
The below getLoggerLvl function will create a logger class for the module that is passesd and stores all the log data into
a file with  format "simpplr_connector_run_time_and_date". The logger implements the file handler , and all the logging overwritten to
the specified file.
'''

def getLoggerLvl(class_name):
    dic_of_options = confParser.config_section_map('file_locations')
    base_log_dir = dic_of_options.get("log_dir")

    logger = logging.getLogger(class_name)
    logger.setLevel(logging.DEBUG)

    log_file_dir_ = base_log_dir + "/" + "simpplr_cdc_{}_{}.log".format(time_string, u_id)
    file_handler =  logging.FileHandler(log_file_dir_, encoding=None, delay=False)

    file_handler.setLevel(logging.DEBUG)
    log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)

    return logger




