import configparser
import sys

config = None
config_file_path = None

config = configparser.ConfigParser()


def set_config():
    config.read(config_file_path)


def config_section_map(section):
    global config
    conf = {}
    options = config.options(section)
    for option in options:
        try:
            conf[option] = config.get(section, option)
            if conf[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            conf[option] = None
            sys.exit(1)

    return conf



