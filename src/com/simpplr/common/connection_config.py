import os

KAFKA_CONSUMER_GROUP = os.getenv('KAFKA_CONSUMER_GROUP', "simpplr_default_cdc_group")

class Connections:
    KAFKA_BROKER: str = os.getenv('KAFKA_BROKER', "PLAINTEXT://localhost:9092")
    REST_PROXY: str = os.getenv('REST_PROXY', "http://rest-proxy:8082")
    SCHEMA_REGISTRY: str = os.getenv('SCHEMA_REGISTRY', "http://schema-registry:8081")
    CONNECT: str = os.getenv('CONNECT', "http://localhost:8083")
    KSQL: str = os.getenv('KSQL', "http://ksql:8088")
