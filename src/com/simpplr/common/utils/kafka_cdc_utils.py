import requests
from sure import expect
import httpretty
import json
import os
from pathlib import Path


PROJECT_HOME = Path(__file__).parent.parent.parent.parent.parent.parent # This is your Project
CONF_PATH = PROJECT_HOME/"conf"

class ProducerUtils:

    def __init__(self,logger):
        self.__logger = logger

    def get_environment_variables(self,propertyFileName)->dict:
        self.__logger.info("Inside function [get_environment_variables] under the class [kafka_cdc_utils]")
        self.__logger.info("Getting all the environment variables and creating a dictionary")
        env_dict = dict(os.environ)
        connector_type = "_".join(propertyFileName.split("_")[:2])
        self.__logger.info("The connector type is : {}".format(connector_type))
        self.__logger.info("Begining to filter the environment variables whose name starts with [ {} ]".format(connector_type.upper()))
        filtered_env_dict = {k: v for k, v in env_dict.items() if k.startswith(connector_type.upper())}
        self.__logger.info("List of environmental variables for connector type {} is as -> {}".format(connector_type,filtered_env_dict.items()))
        return filtered_env_dict

    def get_mapping_keys_for_env_properties(self,propertyFileName,mappingFileName="connector_key_mappings_with_os_environment.json")->dict:
        key_map_dict = dict()
        connector_type = "_".join(propertyFileName.split("_")[:2])
        self.__logger.info("The connector type is : {} inside the function [get_mapping_keys_for_env_properties]".format(connector_type))
        env_variable_mapping_dict_file = "{}/{}".format(CONF_PATH,mappingFileName)
        self.__logger.info("The path for the env mapping file is : {}".format(env_variable_mapping_dict_file))
        with open(env_variable_mapping_dict_file,'r',encoding='utf-8') as env_map_json_file:
            env_key_map_data = json.load(env_map_json_file)
            for connector in env_key_map_data.get("connectors"):
                if(connector.get("connector_name") in connector_type):
                    key_map_dict = connector.get("connector_key_mappings")
                    self.__logger.info("The list of env properties for connector type {} is as {} -> ".format(connector_type,key_map_dict))
        return key_map_dict

    def update_env_key_with_actuals(self,env_dict,map_dict)->dict:
        self.__logger.info("Input for the env_dict is -< {}".format(env_dict))
        self.__logger.info("Input for the map_dict is -< {}".format(map_dict))
        updated_env_key_actuals = dict((map_dict[key], value) for (key, value) in env_dict.items())
        self.__logger.info("After merging env Key property names with actuals is as shown in list is {} -> ".format((updated_env_key_actuals)))
        return updated_env_key_actuals

    def parse_line(self,input):
        key, value = input.split('=')
        key = key.strip()  # handles key = value as well as key=value
        value = value.replace('"',"").strip()
        self.__logger.info("Parsing Properties Files with pair of KEY: {} -> VALUE: {}".format(key,value))
        return key, value

    def get_response_data_for_connector(self,filePath, fileName):
        data = dict()
        self.__logger.info("PROPERTIES FILE NAME LOCATION -> {}/{}.properties".format(filePath, fileName))
        with open('{}/{}.properties'.format(filePath, fileName), 'r') as fp:
            for line in fp:
                line = line.strip()
                if not line or line.startswith('#'):
                    continue

                key, value = self.parse_line(line)
                data[key] = value
            fp.close()
        return data

    def merge_two_dict(self,dict1,dict2):
        merged_dict = {**dict1, **dict2}
        return merged_dict

    def validate_post_request(self,connetor_uri, connector_name, connector_properties_path, connector_file_name):
        CONNECTOR_NAME = connector_name
        CONNECTOR_URL = str(connetor_uri)
        self.__logger.info("The CONNECTOR_URL is : {}".format(CONNECTOR_URL))
        env_dict = self.get_environment_variables(connector_file_name)
        map_dict = self.get_mapping_keys_for_env_properties(connector_file_name)
        connector_type = "_".join(connector_file_name.split("_")[:2])
        data1 = self.get_response_data_for_connector(connector_properties_path, connector_file_name)
        self.__logger.info("The list of default key , values for connector {} is as -> {} ".format(connector_type,data1))
        data2 = self.update_env_key_with_actuals(env_dict,map_dict)
        self.__logger.info("The list of environment key , values for connector {} is as -> {} ".format(connector_type,data2))

        data = self.merge_two_dict(data1,data2)
        self.__logger.info("The final data object list used to send post for connector service is -> {} ".format(data))

        response = requests.post(
            "{}/connectors".format(CONNECTOR_URL),
            headers={'Content-Type': 'application/json'},
            data=json.dumps(
                {
                    "name": CONNECTOR_NAME,
                    "config": data
                }
            ),
        )
        self.__logger.info("POST request response status code is : {}".format(response.status_code))
        return response

    @httpretty.activate
    def validate_connector_exists(self,connetor_uri, connector_name):
        httpretty.register_uri(httpretty.HTTPretty.GET, "{}/{}".format(connetor_uri, connector_name),
                               status=200)
        self.__logger.info("CONNECTOR_URI : {}/{}".format(connetor_uri, connector_name))
        response = requests.get("{}/{}".format(connetor_uri, connector_name))
        expect(response.status_code).to.equal(200)














