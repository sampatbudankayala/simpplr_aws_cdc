#!/bin/bash

SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname ${SCRIPT_PATH}`


while([ -h "${SCRIPT_PATH}" ]); do
    cd "`dirname "${SCRIPT_PATH}"`"
    user_path="$(readlink "`basename "${SCRIPT_PATH}"`")"
done
cd "`dirname "${SCRIPT_PATH}"`" > /dev/null

PROJECT_HOME=$(dirname $(dirname  $(readlink -f $SCRIPT_HOME)))
PROJECT_NAME=`basename ${PROJECT_HOME}`
BIN_HOME="${PROJECT_HOME}/bin/"
COMMON_BIN_HOME="${PROJECT_HOME}/bin/common"
CONSUMER_BIN_HOME="${PROJECT_HOME}/bin/consumer"

. $COMMON_HOME/shell_functions/common_functions.sh

info "PROJECT_HOME=${PROJECT_HOME}"
info "PROJECT_NAME=${PROJECT_NAME}"
info "COMMON_BIN_HOME=${COMMON_BIN_HOME}"
info "CONSUMER_BIN_HOME=${CONSUMER_BIN_HOME}"

info "Reading parameters from the command line"

readParameters "@"

info "Checking if the Variable are set or not"

checkVariableIfSet "CONSUMER_INI_FILE_NAME" "${CONSUMER_INI_FILE_NAME}"
validateExitStatus $? "Variable Exists !! Hence calling the main function" "Variable Does not exists to intitate a script !! Hence exiting"
checkVariableIfSet "KAFKA_TOPIC_NAME" "${KAFKA_TOPIC_NAME}"
validateExitStatus $? "Variable Exists !! Hence calling the main function" "Variable Does not exists to intitate a script !! Hence exiting"

python ${PROJECT_HOME}/src/com/simpplr/consumer_launcher.py --conf_file_path ${PROJECT_HOME}/conf/${CONSUMER_INI_FILE_NAME} --kafka_topic_name ${KAFKA_TOPIC_NAME}

validateExitStatus $? "Python Process for Consumer has been initated for the topic ${KAFKA_TOPIC_NAME}"


