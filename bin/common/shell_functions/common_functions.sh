#!/bin/bash


# Convert the Input string to lower case
function convertToLowerCase() {
    OUTPUT_STRING=`echo $1 | tr '[:upper:]' '[:lower:]'`
    echo $OUTPUT_STRING
}

# Convert the Input string to UPPER case
function convertToUpperCase() {
    OUTPUT_STRING=`echo $1 | tr '[:lower:]' '[:upper:]'`
    echo $OUTPUT_STRING
}

function echoCommand() {
    echo "Executing Command : $1"
}

function execute() {
    evalString1="$1"
    echo -e "Executing Command : $evalString1" | sed 's/ \+ /\t/g' | tr "\t" "\n" |sed "/^$/d"| sed ':a;N;$!ba;s/\n/\n\t\t         /g'
    eval $1
}
#For information
function info() {
    echo -e "[INFO] $1"
}

#For Error
function error() {
    echo -e "[ERROR] $1"
}

#For Error
function warn() {
    echo -e "[WARN] $1"
}

#Current date time
function getFormattedCurrentDate() {
    current_timestamp="`date +"%Y-%m-%d %H:%M:%S"`";
    echo ${current_timestamp}
}

function getFormattedDate() {
    formattedDate=$(date -d $1 +"%Y%m%d")
    echo ${formattedDate}
}

function readParameters() {
    info "Parsing args ..."
    for item in "$@"; do
        case $item in
        (*=*) eval $item;;
        esac
    done
    info "Done parsing args ..!!!"
}

#Checks the exit status
function validateExitStatus() {
    if [ $1 -eq 0 ];
    then
        info "$2";
    else
        error "$3";
        exit 100;
    fi
}

# Exit function to be called if variable is not set by printing the variable name
function variablesNotSetExit() {
    NOT_SET_VARIABLE_NAME=$1
    error "$NOT_SET_VARIABLE_NAME is not set. Validate the options."
    exit 1;
}

# Check if the given variable is set or not
function checkVariableIfSet() {
    VARIABLE_NAME=$1
    VARIABLE_VALUE=$2
    if [ ! -z "$VARIABLE_VALUE" ]; then
        info "$VARIABLE_NAME set to $VARIABLE_VALUE"
    else
        variablesNotSetExit $VARIABLE_NAME
fi
}

function checkVariableIfSetCondition() {
    VARIABLE_VALUE=$1
    if [ ! -z "$VARIABLE_VALUE" ]; then
        return 0
    else
        return 1
    fi
}

# Check exit code of the command
function checkAndLogExistStatus() {
    MESSAGE=$1
    RETURN_CODE=$2
    if [ $RETURN_CODE -eq 0 ]
    then
        info "$MESSAGE successful"
    else
        error "$MESSAGE failed"
        exit $RETURN_CODE
    fi
}


# Load configuration from file
function loadConfigsFromFile() {
    CONFIG_FILE_PATH=$1
    if checkFileExist $CONFIG_FILE_PATH;then
    . $CONFIG_FILE_PATH
        info "Successfully loaded $CONFIG_FILE_PATH"
    else
        warning "$CONFIG_FILE_PATH does not exist. Switching to use defaults."
    fi
}

function replaceDotWithUnderscore() {
    INPUT_STRING=$1
    OUTPUT_STRING=`echo $1 | sed 's/\./_/g'`
    echo $OUTPUT_STRING
}

function getYearFromDate () {
    YEAR=`echo $1|cut -d'-' -f 1`
    echo $YEAR
}

function getMonthFromDate () {
    MONTH=`echo $1|cut -d'-' -f 2`
    echo $MONTH
}

function getDayFromDate() {
    DAY=`echo $1|cut -d'-' -f 3`
    echo $DAY
}

function getYearMonthDayFromDate () {
    #PASSED_DATE=$1;
    export $2=`getYearFromDate $1`
    export $3=`getMonthFromDate $1`
    export $4=`getDayFromDate $1`

}

function checkFileExists() {
    FILE=$1
    if [ -f "$FILE" ]; then
      info "$FILE exist!!!"
    else
      error "$FILE does not exist!!! Hence exiting"
      exit 1;
    fi
}

function createLogDirIfNotExists(){

    BASE_LOG_DIR=$1
    SERVICE_NAME=$2
    checkVariableIfSet "BASE_LOG_DIR" "${BASE_LOG_DIR}"

    if [ ! -d "${BASE_LOG_DIR}/${SERVICE_NAME}" ]; then

        mkdir -p ${BASE_LOG_DIR}/${SERVICE_NAME}
        exit_code=$?
        success_message="Created log directory ${BASE_LOG_DIR}/${SERVICE_NAME}"
        failure_message="Failed to create log directory ${BASE_LOG_DIR}/${SERVICE_NAME}"

        validateExitStatus "${exit_code}" "${success_message}" "${failure_message}"
    fi

}
