#!/bin/bash

SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname ${SCRIPT_PATH}`


while([ -h "${SCRIPT_PATH}" ]); do
    cd "`dirname "${SCRIPT_PATH}"`"
    user_path="$(readlink "`basename "${SCRIPT_PATH}"`")"
done
cd "`dirname "${SCRIPT_PATH}"`" > /dev/null

PROJECT_HOME=$(dirname $(dirname  $(readlink -f $SCRIPT_HOME)))
PROJECT_NAME=`basename ${PROJECT_HOME}`
BIN_HOME="${PROJECT_HOME}/bin/"
COMMON_BIN_HOME="${PROJECT_HOME}/bin/common"
CONNECTOR_BIN_HOME="${PROJECT_HOME}/bin/connector"
BASE_LOG_DIR="${PROJECT_HOME}/log/"

export BASE_LOG_DIR


. $COMMON_BIN_HOME/shell_functions/common_functions.sh

info "PROJECT_HOME=${PROJECT_HOME}"
info "PROJECT_NAME=${PROJECT_NAME}"
info "COMMON_BIN_HOME=${COMMON_BIN_HOME}"
info "CONNECTOR_BIN_HOME=${CONNECTOR_BIN_HOME}"
info "BASE_LOG_DIR=${BASE_LOG_DIR}"

info "Reading parameters from the command line"

readParameters "$@"

info "Checking if the Variable are set or not"

checkVariableIfSet "CONNECTOR_INI_FILE_NAME" "${CONNECTOR_INI_FILE_NAME}"
validateExitStatus $? "Variable Exists !! Hence calling the main function" "Variable Does not exists to intitate a script !! Hence exiting"
checkVariableIfSet "CONNECTOR_PROPERTY_FILE_NAME" "${CONNECTOR_PROPERTY_FILE_NAME}"
validateExitStatus $? "Variable Exists !! Hence calling the main function" "Variable Does not exists to intitate a script !! Hence exiting"
checkVariableIfSet "CONNECTOR_TYPE" "${CONNECTOR_TYPE}"
validateExitStatus $? "Variable Exists !! Hence calling the main function" "Variable Does not exists to intitate a script !! Hence exiting"
checkVariableIfSet "CONNECTOR_NAME" "${CONNECTOR_NAME}"
validateExitStatus $? "Variable Exists !! Hence calling the main function" "Variable Does not exists to intitate a script !! Hence exiting"

case $CONNECTOR_TYPE in
    source|sink) info "The connector is of type $CONNECTOR_TYPE";;
    *)           error "The connector type should be either [source] or [sink]]!!! Hence exiting ";exit 1;
esac

info "Checking for  property file  - $CONNECTOR_PROPERTY_FILE_NAME exists!!!"

checkFileExists ${PROJECT_HOME}/resources/connector_properties_files/${CONNECTOR_TYPE}/${CONNECTOR_PROPERTY_FILE_NAME}.properties
createLogDirIfNotExists ${BASE_LOG_DIR} connector_log

export PYTHONPATH=${PROJECT_HOME}

python3 ${PROJECT_HOME}/src/com/simpplr/producer/connector.py --conf_file_path ${PROJECT_HOME}/conf/${CONNECTOR_INI_FILE_NAME} --properties_file_name ${CONNECTOR_PROPERTY_FILE_NAME} --connector_type ${CONNECTOR_TYPE} --connector_name ${CONNECTOR_NAME}

validateExitStatus $? "Python Process Completed for the Connector  $CONNECTOR_NAME"
