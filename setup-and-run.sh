#!/usr/bin/env bash

TRUE_REG='^([tT][rR][uU][eE]|[yY]|[yY][eE][sS]|1)$'
FALSE_REG='^([fF][aA][lL][sS][eE]|[nN]|[nN][oO]|0)$'

DEBUG_SCRIPT=${DEBUG_SCRIPT:-false}
if [[ $DEBUG_SCRIPT =~ $TRUE_REG ]]; then
    set -o xtrace
    printenv
fi

STRICT_SCRIPT=${STRICT_SCRIPT:-true}
if [[ $STRICT_SCRIPT =~ $TRUE_REG ]]; then
    set -o errexit
    set -o nounset
    set -o pipefail
fi


# Default Kafka Connection env
export KAFKA_BROKER=${KAFKA_BROKER:-"PLAINTEXT://localhost:9092"}
export REST_PROXY=${REST_PROXY:-"http://rest-proxy:8082"}
export SCHEMA_REGISTRY=${SCHEMA_REGISTRY:-"http://schema-registry:8081"}
export CONNECT=${CONNECT:-"http://localhost:8083"}
export KSQL=${KSQL:-"http://ksql:8088"}

#Kafka Consumer group properties
export KAFKA_CONSUMER_GROUP=${KAFKA_CONSUMER_GROUP:-"simpplr_default_cdc_group"}

#Debezium Mysql env variables

export MYSQL_DBEZIUM_CONNECTOR_CLASS="io.debezium.connector.mysql.MySqlConnector"
export MYSQL_DBEZIUM_TASKS_MAX="1"
export MYSQL_DBEZIUM_DATABASE_HOSTNAME="Sampat.local"
export MYSQL_DBEZIUM_DATABASE_PORT="3306"
export MYSQL_DBEZIUM_DATABASE_USER="root"
export MYSQL_DBEZIUM_DATABASE_PASSWORD="password"
export MYSQL_DBEZIUM_DATABASE_SERVER_ID="223344"
export MYSQL_DBEZIUM_DATABASE_SERVER_NAME="mysql-localhost"
export MYSQL_DEBEZIUM_DATABASE_WHITELIST="simpplr"
export MYSQL_DBEZIUM_DATABASE_HISTORY_KAFKA_BOOTSTRAP_SERVERS="localhost:9092"
export MYSQL_DBEZIUM_DATABASE_HISTORY_KAFKA_TOPIC="mysql-schema-simpplr"
export MYSQL_DBEZIUM_KEY_CONVERTER_SCHEMAS_ENABLE="false"
export MYSQL_DBEZIUM_VALUE_CONVERTER_SCHEMAS_ENABLE="false"
export MYSQL_DBEZIUM_VALUE_CONVERTER="org.apache.kafka.connect.json.JsonConverter"
export MYSQL_DBEZIUM_KEY_CONVERTER="org.apache.kafka.connect.json.JsonConverter"

#Shell argument values
export CONNECTOR_INI_FILE_NAME="cdc_producer_config.ini"
export CONNECTOR_PROPERTY_FILE_NAME="mysql_debezium_jdbc_connection"
export CONNECTOR_TYPE="source"
export CONNECTOR_NAME="my_test_connector"