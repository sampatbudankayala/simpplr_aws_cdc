FROM ubuntu:18.04
MAINTAINER sampat.budankayala@simpplr.com


RUN apt-get update -y && apt-get install python3.8 -y && apt install python3-pip -y && apt install build-essential automake pkg-config libtool libffi-dev libgmp-dev -y && apt install libsecp256k1-dev -y
RUN apt-get update && apt-get install -y openjdk-8-jre && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN apt-get update -y && apt install git -y
RUN apt-get update -y && apt-get install libkrb5-dev -y
RUN apt-get update -y && apt-get install vim -y


RUN mkdir /opt/app
RUN chown -R root:root /opt/app
COPY ["requirements.txt","/opt/app/requirements.txt"]
SHELL ["/bin/bash", "-c"]

WORKDIR /opt/app

RUN mkdir /opt/app/simpplr_aws_cdc
RUN chown -R root:root /opt/app/simpplr_aws_cdc

RUN cd /opt/app/simpplr_aws_cdc

RUN git clone -b master-dokcer-env https://bitbucket.org/sampatbudankayala/simpplr_aws_cdc.git
RUN pip3 install -r requirements.txt

ENV CONNECTOR_INI_FILE_NAME=${CONNECTOR_INI_FILE_NAME}
ENV CONNECTOR_PROPERTY_FILE_NAME=${CONNECTOR_PROPERTY_FILE_NAME}
ENV CONNECTOR_TYPE=${CONNECTOR_TYPE}
ENV CONNECTOR_NAME=${CONNECTOR_NAME}

CMD ["bash","/opt/app/simpplr_aws_cdc/bin/connector/connector.sh","CONNECTOR_INI_FILE_NAME=${CONNECTOR_INI_FILE_NAME}","CONNECTOR_PROPERTY_FILE_NAME=${CONNECTOR_PROPERTY_FILE_NAME}","CONNECTOR_TYPE=${CONNECTOR_TYPE}","CONNECTOR_NAME=${CONNECTOR_NAME}"]
